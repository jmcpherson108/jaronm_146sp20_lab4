/**
 * Class definition for an abstract Animal -- it's abstract because
 * we want the animal to speak (when commanded to do so), but we don't
 * know exactly what TYPE of animal it will be, and so we don't know
 * what it will say! (The implementation of the speak() method
 * will be handled by various subclasses of Animal.)
 *
 * And because it is INCOMPLETE, an abstract class
 * CANNOT BE INSTANTIATED! Do not forget this, EVER!!
 *
 * @author username@email.uscb.edu
 * @version 146sp20 Lab 4
 */
// Because Animal implements Speakable, Animal now has
// inherited the public abstract method speak() from Speakable.
// since we have not provide a concrete implementation of
// the speak() method here in Animal, then Animal must be 
// declared abstract.
public abstract class Animal implements Speakable{
    
    /* FIELDS / INSTANCE VARIABLES */
    private String name;
    private int speakableTypeNum;
    
    /* CONSTRUCTORS */
    public Animal( String name, int speakableTypeNum )
    {
        this.name = name;
        this.speakableTypeNum = speakableTypeNum;
    } // end 2-arg constructor
    
    /* METHODS */
    /**
     * @return the name
     */
    public String getName() {
        return name;
    } // end CONCRETE method getName

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    } // end CONCRETE method setName
    
    // Since the METHOD is declared abstract, 
    // the containing CLASS must *also* be declared abstract!
    //public abstract void speak();

@Override
public String toString(){
    
    String speakableTypeStr = "Unknown"; // for initialization only
    
    switch (speakableTypeNum) {
        case DOG:
            speakableTypeStr = "dog";
            break;
        case CAT:
            speakableTypeStr = "cat";
            break;
        case MOUSE:
            speakableTypeStr = "mouse";
            break;
        case FOX:
            speakableTypeStr = "fox";
            break;
        default:
            break;
    } // end switch
    
    return String.format(
        "My name is %s and I'm a %s! ", name, speakableTypeStr);
    
} // end OVERRIDING method toString

} // end ABSTRACT class Animal

