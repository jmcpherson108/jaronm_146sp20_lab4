/**
 * Fox class that implements the speak() method of the
 * abstract Animal superclass
 *
 * @author username@email.uscb.edu
 * @version 146sp20 Lab 4
 */
public class Fox extends Animal {

    /* CONSTRUCTOR(S) */
    public Fox( String name, int speakableTypeNum )
    {
        // pass name up to the superclass constructor
        // ("You can't make a Fox without making an Animal first." However, note 
        //  that we are NOT instantiating an Animal; we are still instantiating 
        //  a Fox object, but by calling the *superclass* constructor, we can take
        //  advantage of the Animal's constructor to *initialize* the Fox's name!)
        // RELATED NOTE: Constructors are NEVER inherited in Java, even if public!        
        super( name, speakableTypeNum );
        
        // any additional initializations specific to a Fox
        // object would need to be coded AFTER the call
        // to the superclass constructor
        
    } // end 2-arg constructor

    /* METHODS */
    @Override
    public void speak() {
        // What does the fox say?
        System.out.println("Ring-ding-ding-ding-dingeringeding!!");

    } // end CONCRETE method speak

} // end CONCRETE class Fox

