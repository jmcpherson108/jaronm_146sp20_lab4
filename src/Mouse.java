/**
 * Mouse class that implements the speak() method of the
 * abstract Animal superclass
 *
 * @author username@email.uscb.edu
 * @version 146sp20 Lab 4
 */
public class Mouse extends Animal {

    /* CONSTRUCTOR(S) */
    public Mouse( String name, int speakableTypeNum )
    {
        // pass name up to the superclass constructor
        // ("You can't make a Mouse without making an Animal first." However, note 
        //  that we are NOT instantiating an Animal; we are still instantiating 
        //  a Mouse, but by calling the *superclass* constructor, we can take
        //  advantage of the Animal constructor to *initialize* the Mouse's name!)
        // RELATED NOTE: Constructors are NEVER inherited in Java, even if public!        
        super( name, speakableTypeNum );
        
        // any additional initializations specific to a Mouse
        // object would need to be coded AFTER the call
        // to the superclass constructor
        
    } // end 2-arg constructor

    /* METHODS */
    
    @Override
    public void speak() {
        // What does a mouse say?
        System.out.println("Squeak!");

    } // end CONCRETE method speak

} // end CONCRETE class Mouse
