/**
 * Test class to illustrate polymorphic behavior at runtime
 *
 * @author username@email.uscb.edu
 * @version 146sp20 Lab 4
 */
public class RuntimePolymorphismDemo {

    public static void main(String[] args) {
        // Here we create a 5-element, 1-D array of Speakable object references
        Speakable[] speakableObjects = new Speakable[5];

        // ...and to each array element, we assign a reference
        // to a newly instantiated object of each Animal subclass
        speakableObjects[0] = new Dog("Spike", Speakable.DOG);
        speakableObjects[1] = new Cat("Tom", Speakable.CAT);
        speakableObjects[2] = new Mouse("Jerry", Speakable.MOUSE);
        speakableObjects[3] = new Fox("Todd", Speakable.FOX);
        speakableObjects[4] = new Computer();

        /*
        Now, we will loop through the animals array... note that nowhere in the 
        for loop do we EXPLICITLY refer to a dog, cat, mouse, or fox object!
        */
        for ( Speakable thisSpeakableObject : speakableObjects ) {
            //display the String representation of this object
            //System.out.print( thisAnimal ); //toString() is called IMPLICITLY
            
            if (  !(thisSpeakableObject instanceof Computer)  ){
                System.out.print( thisSpeakableObject );
            } // end if
            
            // Polymorphically call the speak() method for the specific type 
            // of animal -- the choice of speak() method is determined at
            // runtime (execution time), not compile time! Thus, we call 
            // this an example of RUNTIME polymorphism.
            thisSpeakableObject.speak();

            /*
             (NOTE: In contrast, COMPILE-TIME polymorphism is just another 
              way of saying "method overloading". In RUNTIME polymorphism, we're 
              not over-LOADING methods -- rather, we are over-RIDING them!)
             */
        } // end for

    } // end method main

} // end test class RuntimePolymorphismDemo
