/**
 *
 * @author jaronm
 */
public interface Speakable {
    
    /* FIELDS */
    // interface fields are implicitly public, static, and final
    int DOG = 1;
    int CAT = 2;
    int MOUSE = 3;
    int FOX = 4;
    int OTHER = 5;
    
    /* METHODS*/
    //void speak();
    
    // Starting with Java 8, we can now provide interface
    // with default concrete method implementations...
    default void speak() {
        // we don't NEED to call the toString() method on this
        // object, because it will be called IMPLICITLY...
        // ...but i'm leaving it here as sort of a "breadcrumb"
        // to remind you that a string representation of the object
        // will be displayed when an object reference is passed to
        // System.out.println (which accepts a String)
        System.out.println(this.toString() );
    } // end default message speak
    
} // end interface Speakable
